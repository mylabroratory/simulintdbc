# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.
"""


from jcdb.JcMakeQuery import JcMakeSelectQuery, JcMakeInsertQuery, JcMakeUpdateQuery, JcMakeDDLQuery




class JcConnections():
    __dblabel = ""
    __dbtype = ""
    __m_db_main = None
    __lastSQL = ""

    def __init__(self, dsn):
        if dsn['db'] == 'mysql':
            from jcdb.JcMysql import JcMysql
            self.__m_db_main = JcMysql(dsn)
            self.__dbtype = 'mysql'
            self.__dblabel = 'MySQL'
        elif dsn['db'] == 'firebird':
            from jcdb.JcFirebird import JcFirebird
            self.__m_db_main = JcFirebird(dsn)
            self.__dbtype = 'firebird'
            self.__dblabel = 'Firebird'
        elif dsn['db'] == 'postgres':
            from jcdb.JcPostgres import JcPostgres
            self.__m_db_main = JcPostgres(dsn)
            self.__dbtype = 'postgres'
            self.__dblabel = 'PostgreSQL'
        elif dsn['db'] == 'msaccess':
            from jcdb.JcMsAccess import JcMsAccess
            self.__m_db_main = JcMsAccess(dsn)
            self.__dbtype = 'msaccess'
            self.__dblabel = 'Ms-Access'
        elif dsn['db'] == 'sqlite':
            from jcdb.JcSqlite import JcSqlite
            self.__m_db_main = JcSqlite(dsn)
            self.__dbtype = 'sqlite'
            self.__dblabel = 'SQLite3'
        elif dsn['db'] == 'csv':
            from jcdb.JcCSVFiles import JcCSVFiles
            self.__m_db_main = JcCSVFiles(dsn)
            self.__dbtype = 'csv'
            self.__dblabel = "File Data CSV"
            

    def db_type(self):
        return self.__dbtype
    def db_label(self):
        return self.__dblabel
        
    def connect(self):
        self.__m_db_main.connect()

    def close(self):
        self.__m_db_main.close()

    def exec_command(self, sql):
        data = self.__m_db_main.exec_command(sql)
        return data

    def get_name_columns(self, _table):
        data = self.__m_db_main.get_name_columns(_table)
        return data

    def tables(self):
        data = self.__m_db_main.tables()
        return data

class JcSRecord(JcMakeSelectQuery):
    __dbCommand = None
    __dbConnection = None
    def __init__(self, dbConnection):
        super().__init__( dbConnection )
        self.__dbConnection = dbConnection
                
class JcIRecord(JcMakeInsertQuery):
    __dbCommand = None
    __dbConnection = None
    def __init__(self, dbConnection):
        super().__init__( dbConnection )
        self.__dbConnection = dbConnection

class JcURecord(JcMakeUpdateQuery):
    __dbCommand = None
    __dbConnection = None
    def __init__(self, dbConnection):
        super().__init__(dbConnection)
        self.__dbConnection = dbConnection

class JcSqlDDL(JcMakeDDLQuery):
    __dbCommand = None
    __dbConnection = None
    def __init__(self, dbConnection):
        super().__init__(dbConnection)
        self.__dbConnection = dbConnection






