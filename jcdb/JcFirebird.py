
import sys
import fdb



class JcFirebird:
    __db_firebird = None
    __host = ''
    __port = 0
    __db = ''
    __user = ''
    __passwd = ''
    __charset = ''

    def __init__(self, dsn):
        self.__host =    dsn['host']
        self.__port =    dsn['port']
        self.__db =      dsn['dbname']
        self.__user =    dsn['user']
        self.__passwd =  dsn['passwd']
        self.__charset = dsn['charset']

    def connect(self):
        try:
            __dsn = "{}:{}".format(self.__host, self.__db)
            self.__db_firebird = fdb.connect( dsn= __dsn, user=self.__user, password=self.__passwd )
            #                     fdb.connect( dsn='bison:/temp/test.db', user='sysdba', password='pass')}
        except fdb.Error as e:
            print ("Error {}: {}".format(e.args[0], e.args[1]) )
            sys.exit(1)

    def close(self):
        self.__db_firebird.close()



