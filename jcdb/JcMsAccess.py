# -*- coding: utf-8 -*-

import sys
# import unicodedata
import pyodbc

#class JcMysqlMakeSQL:
#    def __init__(self):
#        print ("def __init__(self):")
'''
connStr = (
    r"Driver={Microsoft Access Driver (*.mdb)};"
    r"Dbq=C:\whatever\mydatabase.mdb;"
    r"SystemDB=C:\whatever\mydatabase.mdw;"
    r"UID=yourUserName;"
    r"PWD=yourPassword;"
    )
'''

class JcMsAccess:
    __db_msaccess = None
    __db = ''
    __systemdb = ''
    __user = ''
    __passwd = ''
    __charset = ''
#    __sqlCommand = JcMysqlMakeSQL
    def __init__(self, dsn):
        self.__connStr = "Driver={Microsoft Access Driver (*.mdb)};"
        self.__connStr += "Dbq={};".format( dsn['dbname'] )
        if dsn['dbsystem'] not in '':
            self.__connStr += "SystemDB=%s;" % dsn['dbsystem']
        if dsn['user'] not in '':
            self.__connStr += "UID=%s;" % dsn['user']
            self.__connStr += "PWD=%s;" % dsn['passwd']



    def connect(self):
        try:
            #print (self.__connStr)
            self.__db_msaccess = pyodbc.connect( self.__connStr )
        except pyodbc.Error as e:
            print ("Error {}: {}".format(e.args[0], e.args[1]) )
            sys.exit(1)

    def close(self):
        self.__db_msaccess.close()
        
    def exec_command(self, _sql_command):
        cursor = self.__db_msaccess.cursor()         # Crear un cursor 
        cursor.execute( _sql_command)          # Ejecutar una consulta 
        if _sql_command.upper().startswith('SELECT'): 
            data = cursor.fetchall()   # Traer los resultados de un select 
            data = tuple(data)  
        else:
            self.__db_msaccess.commit()
            data = None
        return data

    def get_name_columns(self, _table):
        __nc = ()
        cursor = self.__db_msaccess.cursor()
        for row in cursor.columns(table=_table ):
            __nc +=  ( row.column_name, ) 
        return __nc
        
    def tables(self):
        cursor = self.__db_msaccess.cursor()
        tables = cursor.tables(tableType='TABLE')
        return tables
    
        
        
           
"""
        self.__connStr = (
            r"Driver={Microsoft Access Driver (*.mdb)};"
            r"Dbq=C:\whatever\mydatabase.mdb;"
            r"SystemDB=C:\whatever\mydatabase.mdw;"
            r"UID=yourUserName;"
            r"PWD=yourPassword;"
            )
"""
 
