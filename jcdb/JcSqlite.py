#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 07 01:10:30 2020

@author: jc
"""


import sys
import sqlite3



class JcSqlite:  
    def __init__(self, dsn):
        self.__db_mysql = None
        self.__db =      dsn['dbname']

    def connect(self):
        try:
            self.__db_sqlite =  sqlite3.connect(self.__db)
        except sqlite3.Error as e:
            print ("Error {}: {}".format(e.args[0], e.args[1]) )
            sys.exit(1)
    
    def close(self):
        self.__db_sqlite.close()
        
    def exec_command(self, _sql_command):
        cursor = self.__db_sqlite.cursor()         # Crear un cursor 
        cursor.execute( _sql_command)          # Ejecutar una consulta 
        if _sql_command.upper().startswith('SELECT'): 
            data = cursor.fetchall()   # Traer los resultados de un select
            data = tuple(data)
        else:
            self.__db_sqlite.commit()
            data = None
        return data

