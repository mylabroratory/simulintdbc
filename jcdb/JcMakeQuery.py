
import numpy as np




class JcMakeSelectQuery():
    def __init__( self, dbConnection, sqlFields="" ):
        self.__m_db_connection = dbConnection
        self.__m_fields = sqlFields
        self.__m_sql = ""
        self.__m_fields = ""
        self.__m_tables = ""
        self.__m_where = ""
        self.__m_leftjoin = ""
        self.__m_rightjoin = ""
        self.__m_orderbyasc = ""
        self.__m_orderbydesc = ""

    def Select(self, sqlfields=""):
        self.__m_fields = sqlfields
        return self

    def From(self, sqltables):
        self.__m_tables = sqltables
        return self

    def Where(self, sqlwhere):
        self.__m_where = sqlwhere
        return self
    
    def LeftJoin(self, sqltable="", sqlwhere=""):
        if len(sqltable) == 0 and len(sqlwhere) == 0:
            self.__m_leftjoin=""
        else:
            self.__m_leftjoin = " LEFT JOIN {} ON {} ".format(sqltable, sqlwhere)
        return self

    def RightJoin(self, sqltable, sqlwhere):
        self.__m_rightjoin = " RIGHT JOIN {} ON {} ".format(sqltable, sqlwhere)
        return self

    def OrderByAsc(self, sqlfields):
        self.__m_orderbyasc = sqlfields
        return self
 
    def OrderByDesc(self, sqlfields):
        self.__m_orderbydesc = sqlfields
        return self

    def Read(self):
        __data = self.__m_db_connection.exec_command(self.Sql())
        return __data
         
    def Sql(self):
        __sql = "SELECT"
        
        
        if self.__m_fields == "":
            __sql = "{} * ".format(__sql)
        else:
            __sql = "{} {} ".format(__sql, self.__m_fields)
        
        if len(self.__m_tables) > 0:
            __sql = "{}FROM {}".format(__sql, self.__m_tables.strip() ) 
        else:
            print( "Ejectuar una exception ")
            return None

        if len(self.__m_leftjoin) > 0:
            __sql = "{}{}".format(__sql, self.__m_leftjoin )

        if len(self.__m_rightjoin) > 0:
            __sql = "{}{}".format(__sql, self.__m_rightjoin )
        
        if  len(self.__m_where) > 0:
            __sql = "{} WHERE {} ".format(__sql, self.__m_where)
        
        if len(self.__m_orderbyasc) > 0 or len(self.__m_orderbydesc) >0: 
            __sql = "{} ORDER BY ".format(__sql)
            
        if len(self.__m_orderbyasc) > 0:
            if ("," in self.__m_orderbyasc):
                __fields = self.__m_orderbyasc.split(",")
                __fields = [w.replace(w,"{} ASC ".format(w.strip())) for w in __fields]
                __fields = ", ".join(__fields)
                __sql = "{}{} ".format(__sql, __fields)
            else:
                __fields = self.__m_orderbyasc.strip()
                __sql = "{}{} ASC ".format(__sql, __fields)
        
        if len(self.__m_orderbyasc) > 0 and len(self.__m_orderbydesc) > 0:
            __sql = "{}, ".format(__sql.strip())
        
        if len(self.__m_orderbydesc) > 0:
            if ("," in self.__m_orderbydesc):
                __fields = self.__m_orderbydesc.split(",")
                __fields = [f.replace(f, "{} DESC".format(f.strip())) for f in __fields]
                __fields = ", ".join(__fields)
            else:
                __fields = self.__m_orderbydesc.strip()
            __sql = "{}{} DESC ".format(__sql, __fields)
        
        self.__m_sql = __sql
        return self.__m_sql
    

                    

class JcMakeInsertQuery():
    def __init__( self, dbConnections ):
        self.__m_db_connection = dbConnections
        self.__m_sql = ""
        self.__m_fields = ""
        self.__m_table = ""
        self.__m_values = ()
    
    def SetTable(self, sqltable):
        self.__m_table = sqltable
    
    def SetFields(self, sqlfields):
        self.__m_fields = sqlfields
        
    def InsertInTo(self, sqltable, sqlfields):
        self.__m_table = sqltable
        self.__m_fields = sqlfields
        return self
    
    def SetValues(self, sqlvalues):
        if type(sqlvalues) is str:
            print ("SetValues: es un String")
            if sqlvalues.count(",") > 0:
                self.__m_values = tuple(sqlvalues.split(","))
            else:
                sqlvalues = f"('{sqlvalues}')"
                self.__m_values = sqlvalues
        elif type(sqlvalues) is list:
            if np.ndim(sqlvalues) == 1:
                if len(sqlvalues) > 1:
                    sqlvalues = tuple(sqlvalues)
                    self.__m_values = sqlvalues
                elif len(sqlvalues) == 1:
                    sqlvalues = f"('{sqlvalues[0]}')"
                    self.__m_values = sqlvalues
                else:
                    self.__m_values = None
                    print( "Error: en Values")
            else:
                self.__m_values = None
                print( "Error: Formato de multiples listas no soportado")
        elif type(sqlvalues) is tuple:
            #print ( "Es una tupla: ", np.ndim(sqlvalues) )
            tdim = np.ndim(sqlvalues)
            if (tdim==1):
                self.__m_values = sqlvalues
            elif(tdim>1):
                if( self.__m_db_connection.db_type() == 'mysql' or
                   self.__m_db_connection.db_type() == 'postgres'):
                    tmp = []
                    for t in sqlvalues:
                        tmp.append("{}".format(t) )
                    self.__m_values = ",".join(tmp)
                elif( self.__m_db_connection.db_type() == 'csv'):
                    tmp = []
                    for t in sqlvalues:
                        tmp.append( t )
                    self.__m_values = tmp
                elif( self.__m_db_connection.db_type() == 'msaccess' or
                     self.__m_db_connection.db_type() == 'firebird' ):
                    raise Exception('Error', 'MS-Access no soporta multiples renglones en un INSERT' )
        return self
        
    def Where(self, sqlwhere):
        self.__m_where = sqlwhere
        return self
    
    def Sql(self):
        __sql = "INSERT INTO {} {} VALUES ".format(self.__m_table, self.__m_fields)
        
        __type = self.__m_db_connection.db_type()
        if __type in ( "mysql", "sqlite" ):
            __sql = "{}{}".format(__sql, self.__m_values )
        else:
            __sql = "{}{}".format(__sql, self.__m_values )
        self.__m_sql = __sql
        
        return self.__m_sql

    def Write(self):
        #print ( self.Sql() )
        __data = self.__m_db_connection.exec_command(self.Sql())
        return __data

    def GetType(self):
        return self.__m_db_connection.db_type()     




class JcMakeUpdateQuery():
    def __init__( self, dbConnections ):
        self.__m_db_connection = dbConnections
        self.__m_sql = ""
        self.__m_fields = ""
        self.__m_table = ""
        self.__m_values = ()
        self.__m_where = ""
    
    def SetTable(self, sqltable):
        self.__m_table = sqltable
    
    def SetFields(self, sqlfields):
        #print( sqlfields )
        #print( len(sqlfields) )
        
        if (type(sqlfields) is tuple):
            if ( len(sqlfields) > 1 or len(sqlfields) < 1):
                raise "Error: Verifique la tupla que indica los campos afectados en la actuliazacion."
            else:
                self.__m_fields = sqlfields
        else:
            raise "Error: Los campos deben estar formateado como una tupla."
        return self
        
    def SetValues(self, sqlvalues):
        if ( type(sqlvalues) is tuple):
            if ( len(sqlvalues) > 1 or len(sqlvalues) < 1):
                raise "Error: No se permite actualizaciones masivas."
            else:
                self.__m_values = sqlvalues
        else:
            raise "Error: Los valores deben formatearce en una tupla."
        self.__m_values = sqlvalues
        return self
        
    def Where(self, sqlwhere):
        self.__m_where = sqlwhere
        return self        
    
    def Sql(self):
        if (self.__m_table == None or self.__m_table == '' ):
            raise "Error: Debe indicar el nombre de una tabla"
        __list = []
        __fcount = len( self.__m_fields )
        __vcount = len( self.__m_values )
        if (__fcount == __vcount):
            for i in range( 0, __fcount):
                __list.append("{}='{}'".format(self.__m_fields[i], self.__m_values[i]) )
            __sql = "UPDATE {} SET {} WHERE {}".format(self.__m_table, "=".join(__list), self.__m_where)
            self.__m_sql = __sql
        else:
            raise "Error: Los cantidad de parametros(campos,valores), no coinsiden."
        return self.__m_sql

    def Write(self):
        print ( self.Sql() )
        __data = self.__m_db_connection.exec_command(self.Sql())
        return __data

    def GetType(self):
        return self.__m_db_connection.db_type()     
    
    
class JcMakeDDLQuery():
    pass

