#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 08 09:37:50 2020

@author: jc
"""


import sys
import MySQLdb



class JcMysql:
    def __init__(self, dsn):
        self.__db_mysql = None
        self.__host =    dsn['host']
        self.__port =    dsn['port']
        self.__db =      dsn['dbname']
        self.__user =    dsn['user']
        self.__passwd =  dsn['passwd']
        self.__charset = dsn['charset']

    def connect(self):
        try:
            self.__db_mysql = MySQLdb.connect( host=self.__host, user=self.__user, passwd=self.__passwd, db=self.__db )
        except MySQLdb.Error as e:
            print ("Error {}: {}".format(e.args[0], e.args[1]) )
            sys.exit(1)

    def close(self):
        self.__db_mysql.close()
        
    def exec_command(self, _sql_command):
        cursor = self.__db_mysql.cursor()         # Crear un cursor 
        cursor.execute( _sql_command)          # Ejecutar una consulta 
        if _sql_command.upper().startswith('SELECT'): 
            data = cursor.fetchall()   # Traer los resultados de un select 
            data = tuple(data)
        else:
            self.__db_mysql.commit()
            data = None
        return data
