# -*- coding: utf-8 -*-

import sys
import psycopg2




class JcPostgres:
    __db_postgres = None
    __host = ''
    __port = 0
    __db = ''
    __user = ''
    __passwd = ''
    __charset = ''
#    __sqlCommand = JcMysqlMakeSQL
    def __init__(self, dsn):
        self.__host =    dsn['host']
        self.__port =    dsn['port']
        self.__db =      dsn['dbname']
        self.__user =    dsn['user']
        self.__passwd =  dsn['passwd']
        self.__charset = dsn['charset']

    def connect(self):
        try:
            self.__db_postgres = psycopg2.connect(host=self.__host, database=self.__db, user=self.__user, password=self.__passwd)            
        except psycopg2.Error as e:
            print ("Error {}: {}".format(e.args[0], e.args[1]) )
            sys.exit(1)

    def close(self):
        self.__db_postgres.close()
        
    def exec_command(self, _sql_command):
        cursor = self.__db_postgres.cursor()
        try:
            cursor.execute( _sql_command )
        except psycopg2.Error as e:
            print ("Error {}: {}".format(e.args[0], e.args[1]) )
            sys.exit(1)
        if _sql_command.upper().startswith( 'SELECT' ):
            data = cursor.fetchall()   
            data = tuple(data)
        else:
            try:
                self.__db_postgres.commit()
                data = None
            except psycopg2.Error as e:
                print ("Error {}: {}".format(e.args[0], e.args[1]) )
                sys.exit(1)
        return data
