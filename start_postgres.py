#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 07:57:24 2020

@author: jc
"""

import os
import json
from JcConnections import JcConnections, JcSRecord, JcIRecord, JcURecord

with open('config.json', 'r') as file:
    cfg = json.load(file)

_dsn = {
    'db'        : 'postgres',
    'host'      : cfg['POSTGRESQL']['db_host'],
    'port'      : cfg['POSTGRESQL']['db_port'],
    'dbname'    : cfg['POSTGRESQL']['db_name'],
    'user'      : cfg['POSTGRESQL']['db_user'],
    'passwd'    : cfg['POSTGRESQL']['db_password'],
    'charset'   : 'utf8',
}


os.system("clear")

print( "Inicio.." )
_db = JcConnections(_dsn)

_db.connect()
ra = JcSRecord( _db )
ra.Select( "regionid, regiondescription" )
ra.From("region")
ra.Where("regionid=1")
#ra.OrderByAsc("ProductID")
#print( ra.Sql() )

print( ra.Read() )


ri = JcIRecord( _db )





_db.close()
print ("Fin")


