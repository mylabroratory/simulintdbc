#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 2 08:06:34 2020

@author: jc
"""


import os
import json
from JcConnections import JcConnections, JcSRecord, JcIRecord, JcURecord

with open('config.json', 'r') as file:
    cfg = json.load(file)

_dsn = {
    'db'        : 'mysql',
    'host'      : cfg['MYSQL']['db_host'],
    'port'      : cfg['MYSQL']['db_port'],
    'dbname'    : cfg['MYSQL']['db_name'],
    'user'      : cfg['MYSQL']['db_user'],
    'passwd'    : cfg['MYSQL']['db_password'],
    'charset'   : 'utf8',
}


os.system("clear")
  
print( "Inicio.." )
_db = JcConnections(_dsn)
_db.connect()
ra = JcSRecord( _db )
ra.Select("productid, productname")
ra.From("Products")


for i in range(2,12):
    ra.Where("productid={}".format(i))
    print(ra.Read()[0])


ru = JcURecord(_db)

ru.SetTable( "Products")
ru.SetFields( ("productname",) ) 
ru.SetValues(( "Esto es el update" ,))

ru.Where("productid=1")

print (ru.Sql())

ru.Write()

print ( ra.Where("productid=1").Read() )



_db.close()
print ("Fin")



