#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 19:36:50 2020

@author: jc
"""


import os
import json
from JcConnections import JcConnections, JcSRecord, JcIRecord, JcURecord

with open('config.json', 'r') as file:
    cfg = json.load(file)

_dsn = {
    'db'        : 'sqlite',
    'dbname'    : cfg['SQLITE']['db_name']
}


os.system("clear")

print( "Inicio.." )
_db = JcConnections(_dsn)
_db.connect()

# instancia un objeto de lectura, crea un SELECT simple
r = JcSRecord( _db )
r.Select()
r.From( "Customers" )
print ( r.Read() )

# instancia un objeto de lectura, crea un SELECT 
r = JcSRecord( _db )
r.Select("CustomerID,CompanyName")
r.From("Customers")
r.OrderByDesc("CompanyName")
for row in r.Read():
    print (row)

# Crea un SELECT con JOIN
r = JcSRecord( _db )
r.Select("p.ProductName, c.CategoryName").From("Products AS p")
r.LeftJoin("Categories AS c", "p.CategoryID=c.CategoryID")

for row in r.Read():
    print (row)


# Crea un INSERT
r = JcIRecord( _db )
r.SetTable("Regions")
r.SetFields(("RegionID", "RegionDescription"))
r.SetValues(("6", "Sur"))
r.Write()
# Consulta el estado
r = JcSRecord( _db )
r.Select().From("Regions")
print( r.Read() )


# Crea una actualizacion
print( "JcURecord" )
r = JcURecord( _db )
r.SetTable("Regions")
r.SetFields(("RegionDescription",))
r.SetValues(("Norte",))
r.Where( "RegionID=6" )
r.Write()

# Consulta el estado
r = JcSRecord( _db )
r.Select().From("Regions")
print( r.Read() )



_db.close()
print ("Fin")


