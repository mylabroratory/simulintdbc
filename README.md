**SiMulIntDBC, es un tooldb que nos permitirá conectarnos a varias bases de datos por medio de una sola interfaz**

SiMulIntDBC: Simplificando Múltiples Interfaces de conexiones a bases de datos.

*Es un desarrollo en python3 con propósitos didácticos, realizado para la materia Programación, de 2do año de la Facultad de Ingeniería, Obera-Misiones.*

---

## Requisitos:

Software necesario para poder ejectuar la tooldb

1. Python3.
2. Modulos de driver para cada base de dato que corresponda.
3. Modulos extras
 - `numpy`
 - `panda`

---

## Compatibilidad

Es compatible con las siguientes bases de datos.

1. **MariaDB**, requiere que este instalado su controlador

 - Linux:
 ` $ sudo apt-get install python-mysqldb`
 
 - Windows
 ` c:\> pip install MySQL-python`

 En caso que falle al instalar el modulo, entonces actualizar pip para realizar una  instalacion manual.
 ` c:\> python -m pip install --upgrade pip`

 Luego bajar el modulo *mysqlclient* de https://www.lfd.uci.edu/~gohlke/pythonlibs/#mysqlclient
 ` c:\> python --version`
 ` c:\> Python 3.8.6`

  La version de paquete que baje y funciono imediatamente fue, *mysqlclient-1.4.6-cp38-cp38-win_amd64.whl* que corresponde para la version de windows y python que tengo.
 
 Luego:
 ` c:\> pip install mysqlclient-1.4.6-cp38-cp38-win_amd64.whl`

2. **MsAccess**, require del paquete de conexion a la odbc

 - Windows:
 ` c:\> pip install pyodbc`

3. **Firebird**, requiere que este instalado su controlador

 ` $ pip install firebirdsql`

4. **PostgreSQL**, requiere que este instalado su controlador

 ` $ pip install psycopg2`
 ` $ pip install pygresql`

5. **Firebird**, requiere que este instalado su controlador

 ` $ pip install firebirdsql`

6. **SQLite3** ya esta instalado por defecto el controlador







